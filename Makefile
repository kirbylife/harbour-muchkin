Appname:=$(shell cat appname.txt)
prefix:=/usr
temp:=/tmp/fpm-jolla
sourcePath:=$(shell pwd)
dependencies=$(shell for file in `cat dependencies.txt`;do echo "-d "$${file};done;)
version:=0.4.5
iteration:=5

all: clean build-tmp rpm-i686 rpm-jolla rpm-aarch64

build-tmp:
	mkdir -p $(temp)/usr/share/applications
	mkdir -p $(temp)/usr/share/icons/hicolor/86x86/apps
	mkdir -p $(temp)/usr/share/$(Appname)/src
	mkdir -p $(temp)/usr/bin
	cp -ar ./qml $(temp)/usr/share/$(Appname)
	cp -ar ./src/* $(temp)/usr/share/$(Appname)/src
	cp ./dat/$(Appname).desktop $(temp)/usr/share/applications/
	cp -ar ./dat/appicon.png $(temp)/usr/share/icons/hicolor/86x86/apps/$(Appname).png
	install -m 755 ./dat/$(Appname).sh $(temp)/usr/bin/$(Appname)

rpm-i686: arch:=i686
rpm-i686: rpmname:=$(Appname)-$(version)-$(iteration).$(arch).rpm
rpm-i686: build-tmp
	cd $(temp);fpm -f -s dir -t rpm \
		--after-install $(sourcePath)/dat/upgradeScript.sh \
		--after-remove $(sourcePath)/dat/removeScript.sh \
		--rpm-changelog $(sourcePath)/changelog.txt \
		--directories "/usr/share/$(Appname)" \
		-v $(version) \
		--iteration $(iteration) \
		$(dependencies) \
		-p $(temp)/$(rpmname) \
		-n $(Appname) \
		-a $(arch) \
		--prefix / *

rpm-jolla: arch:=armv7hl
rpm-jolla: rpmname:=$(Appname)-$(version)-$(iteration).$(arch).rpm
rpm-jolla: build-tmp
	cd $(temp);fpm -f -s dir -t rpm \
		--after-install $(sourcePath)/dat/upgradeScript.sh \
		--after-remove $(sourcePath)/dat/removeScript.sh \
		--rpm-changelog $(sourcePath)/changelog.txt \
		--directories "/usr/share/$(Appname)" \
		-v $(version) \
		--iteration $(iteration) \
		$(dependencies) \
		-p $(temp)/$(rpmname) \
		-n $(Appname) \
		-a $(arch) \
		--prefix / *

rpm-aarch64: arch:=aarch64
rpm-aarch64: rpmname:=$(Appname)-$(version)-$(iteration).$(arch).rpm
rpm-aarch64: build-tmp
	cd $(temp);fpm -f -s dir -t rpm \
		--after-install $(sourcePath)/dat/upgradeScript.sh \
		--after-remove $(sourcePath)/dat/removeScript.sh \
		--rpm-changelog $(sourcePath)/changelog.txt \
		--directories "/usr/share/$(Appname)" \
		-v $(version) \
		--iteration $(iteration) \
		$(dependencies) \
		-p $(temp)/$(rpmname) \
		-n $(Appname) \
		-a $(arch) \
		--prefix / *

clean:
	rm -rf $(temp)
	rm -rf $(builddir)
	rm -rf ./$(Appname)-$(version)-$(iteration)*

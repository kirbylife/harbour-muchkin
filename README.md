# Muchkin icons
==============
Make your webapps icons in the app box look better with the rest of your native apps by choosing whether the corners are rounded or pointed

## Dependencies
- SailfishOS (tested in SailfishOS 4 with an Xperia XA2)
- ~~[Python3-numpy](https://openrepos.net/content/nobodyinperson/python3-numpy)~~
- ~~[Python3-pillow](https://openrepos.net/content/birdzhang/python3-pillow)~~ [Python3-imaging](https://github.com/sailfishos/python3-imaging/) (Jolla oficial PIL package)

## Build dependencies
- [fpm](https://github.com/jordansissel/fpm)
- [rpm-tools](http://rpm.org/)

## Build your own RPM package
1. Clone the repository: `git clone https://gitlab.com/kirbylife/harbour-muchkin`
1. Go to the directory: `cd harbour-muchkin`
1. Build the package.
  1. armv7hl: `make rpm-jolla`
  1. i686: `make rpm-virt`
  1. aarch64: `make rpm-aarch64`
1. the .rpm file will be on `/tmp/fpm-jolla/`

## TO-DO
- [x] Support to aarch64 arquitecture
- [x] Choice non-transparent color to fill the empty spaces
- [x] Fix the issue with sailjail
- [ ] Make betters Make commands
- [ ] Add option to request favicon
- [ ] Add option to restore the original icon
- [ ] Make all icons in the main grid the same size
- [ ] Add option to modify name and link
- [ ] Improve the shitty icon (maybe)
- [ ] Add option to modify the size of the original icon to fit better
- [ ] ...

Contributors are welcome :).

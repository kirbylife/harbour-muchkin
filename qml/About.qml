import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    PageHeader {
        id: header
        title: "About"
    }

    Label {
        anchors.top: header.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        text: "this application was created by\nkirbylife\nunder the GPL V2.0 license,\nanyone is free to view\nthe code and modify it."
        truncationMode: TruncationMode.Elide
    }

    Button {
        id: btnCode
        anchors.bottom: btnTwitter.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 50
        text: "Code"
        onClicked: {
            Qt.openUrlExternally("https://gitlab.com/kirbylife/harbour-muchkin")
        }
    }
    Button {
        id: btnTwitter
        anchors.bottom: btnDonate.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 50
        text: "Twitter"
        onClicked: {
            Qt.openUrlExternally("https://twitter.com/kirbylife")
        }
    }
    Button {
        id: btnDonate
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 50
        text: "Donate"
        onClicked: {
            Qt.openUrlExternally("https://www.paypal.com/paypalme/kirbylife")
        }
    }
}

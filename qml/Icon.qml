import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.3

Page {
    id: iconEditor

    property var attrs
    property var corners: [true, true, true, true]

    PageHeader {
        id: header
        width: parent.width
        title: "name: " + attrs.Name
    }

    SilicaGridView {
        anchors.top: header.bottom
        width: parent.width
        height: width
        cellWidth: width / 2
        cellHeight: cellWidth

        model: ListModel {
            id: gridItems

            ListElement {
                idCell: 0
                activated: true
            }
            ListElement {
                idCell: 1
                activated: true
            }
            ListElement {
                idCell: 2
                activated: true
            }
            ListElement {
                idCell: 3
                activated: true
            }
        }

        delegate: GridItem {
            onClicked: {
                activated = !activated
                iconEditor.corners[idCell] = activated
                py.importModule("main", function(){
                    py.call("main.sailify", [(iconEditor.attrs.old_icon || iconEditor.attrs.Icon).toString(), iconEditor.corners], function(result){
                        icon.source = result
                    })
                })
            }
        }
    }

    Image {
        id: icon
        anchors.top: header.bottom
        width: parent.width
        height: width
    }

    Button {
        anchors.top: icon.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 10
        text: "Save"
        onClicked: {
            if(iconEditor.attrs.old_icon === undefined) {
                iconEditor.attrs.old_icon = iconEditor.attrs.Icon.toString()
            }
            iconEditor.attrs.Icon = icon.source.toString()
            py.importModule("main", function(){
                py.call("main.save_icon", [iconEditor.attrs], function(result){
                    if(result){
                        pageStack.pop()
                        pageStack.completeAnimation()
                        pageStack.currentPage.mainList.reload()
                    }
                })
            })
        }
    }

    Python {
        id: py
        Component.onCompleted: {
            py.addImportPath(Qt.resolvedUrl("../src"));
            py.importModule("main", function(){
                py.call("main.sailify", [(iconEditor.attrs.old_icon || iconEditor.attrs.Icon).toString(), [1, 1, 1, 1]], function(result){
                    icon.source = result
                })
            })
        }
    }
}

import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.3

ApplicationWindow {
    id: root
    cover: Qt.resolvedUrl("Cover.qml")
    Python {
        Component.onCompleted: {
            pageStack.push(Qt.resolvedUrl("Main.qml"));
        }
    }
}

import pyotherside  # pyright: ignore reportMissingImports
from PIL import Image, ImageDraw, ImageChops
from PIL.Image import Image as ImageType
from io import BytesIO
from glob import glob
from collections import Counter
import requests
import base64
import os
import shutil
import logging

logging_filename = os.path.join(os.path.expanduser("~"),
                                ".local",
                                "share",
                                "org.kirbylife",
                                "harbour-muchkin",
                                "muchkin.log")
os.makedirs(os.path.dirname(logging_filename), exist_ok=True)
logging.basicConfig(filename=logging_filename, filemode="w", level=logging.DEBUG)

def download_icon(url) -> ImageType:
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    img = img.convert("RGBA")
    return img

def base64_to_img(base64_bytes: bytes) -> ImageType:
    img_bytes = base64.b64decode(base64_bytes)
    img_buffer = BytesIO(img_bytes)
    img = Image.open(img_buffer)
    img = img.convert("RGBA")
    return img

def avg_colors(img):
    counter = Counter()
    width, height = img.size
    for x in range(width):
        for y in range(height):
            pixel = img.getpixel((x, y))
            if pixel[-1] > 0: # Doesn't count the transparent pixels
                counter[pixel] += 1
    return counter.most_common(1)[0][0]

def add_background(img: ImageType, bg_tuple: tuple) -> ImageType:
    bg = Image.new("RGBA", img.size, bg_tuple)
    bg.paste(img, (0, 0), img)
    return bg

def crop_to_circle(im, template):
    im = im.copy()
    bigsize = (im.size[0] * 3, im.size[1] * 3)
    mask = Image.new('L', bigsize, 0)
    ImageDraw.Draw(mask).ellipse((0, 0) + bigsize, fill=255)
    midsize = tuple(x//2 for x in bigsize)
    if template[0] == 1:
        ImageDraw.Draw(mask).rectangle((0, 0) + midsize, fill=255)
    if template[1] == 1:
        ImageDraw.Draw(mask).rectangle((midsize[0], 0, bigsize[0], midsize[1]), fill=255)
    if template[2] == 1:
        ImageDraw.Draw(mask).rectangle((0, midsize[1], midsize[1], bigsize[1]), fill=255)
    if template[3] == 1:
        ImageDraw.Draw(mask).rectangle(midsize + bigsize, fill=255)
    mask = mask.resize(im.size, Image.ANTIALIAS)
    mask = ImageChops.darker(mask, im.split()[-1])
    im.putalpha(mask)
    return im

def parse_file(desktop_path):
    output = {}
    with open(desktop_path, "r") as f:
        for line in f:
            if not line:
                continue
            if line.startswith("["):
                continue
            key, value = line.split("=", 1)
            output[key] = value.strip()
    output["path"] = desktop_path
    return output

def deparse_file(app):
    output = ["[Desktop Entry]"]
    for key, value in app.items():
        output.append(f"{key}={value}")
    return "\n".join(output)

def get_web_icons():
    path = os.path.join(
        os.path.expanduser("~"),
        ".local",
        "share",
        "applications",
        "sailfish-browser*.desktop"
    )
    apps = glob(path)

    return list(map(parse_file, apps))

def sailify(raw_str, pattern):
    try:
        # New versions of Sailfish has the url on the icons
        img = download_icon(raw_str)
        logging.info("Downloaded from url: " + raw_str)
    except requests.exceptions.InvalidSchema:
        # The old ones converted the icon to base64
        base64_bytes = raw_str.replace("data:image/png;base64,", "", 1).encode()
        img = base64_to_img(base64_bytes)
        logging.info("Converted from base64")
    except Exception as e:
        logging.error(e)
        raise e
    bg_tuple = avg_colors(img)
    img = add_background(img, bg_tuple)
    img = crop_to_circle(img, pattern)
    output_buffer = BytesIO()
    img.save(output_buffer, format="PNG")
    output_data = output_buffer.getvalue()
    output_base64 = base64.b64encode(output_data)
    return "data:image/png;base64," + output_base64.decode()

def backup_icon(app):
    if not os.path.exists(app["path"] + "backup"):
        backup_path = app["path"] + "_backup"
        shutil.copyfile(app["path"], backup_path)
        logging.info("Icon backed up on: " + backup_path)
    else:
        logging.info("Backup already exists")

def save_icon(app):
    new_content = deparse_file(app)
    with open(app["path"], "w") as f:
        f.write(new_content)
    logging.info("Icon saved on: " + app["path"])
    return True

def main():
    import shutil

    while True:
        apps = get_web_icons()
        while True:
            for n, app in enumerate(apps):
                print(f"{n}) {app['Name']}")
            index = input("select an application (the index):\n>>> ")
            index = int(index)
            if index >= len(apps):
                print("Select a valid index")
                continue
            break
        app = apps[index]

        if app["Icon"] == "icon-launcher-bookmark":
            print("this webapp is using the generic icon of a bookmark and not a favicon so it is not possible to modify")
            continue

        while True:
            pattern = input("Enter the pattern (0 = round, 1 = peak):\n>>> ")
            if len(pattern) != 4:
                print("it is necessary to enter 4 characters")
            pattern = tuple(map(int, pattern))
            new_icon = sailify(app.get("old_icon", app["Icon"]), pattern)
            # Backup the original .desktop
            shutil.copyfile(app["path"], app["path"] + "_backup")
            app["old_icon"] = app.get("old_icon", app["Icon"])
            app["Icon"] = new_icon
            new_content = deparse_file(app)
            with open(app["path"], "w") as f:
                f.write(new_content)
            print(f"{app['Name']} sailified correctly")
            break


if __name__ == "__main__":
    main()
